import React, { useState } from "react";
import Square from "./Square";
import "./Board.css";

const Board = () => {
  const [state, setState] = useState(Array(9).fill(null));
  console.log(state, "state");
  const [isXTurn, setIsXTurn] = useState(true);
  const [player1, setPlayer1] = useState("");
  const [player2, setPlayer2] = useState("");
  const [gameStarted, setGameStarted] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const checkWinner = () => {
    const winnerLogic = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let logic of winnerLogic) {
      const [a, b, c] = logic;
      if (state[a] !== null && state[a] === state[b] && state[a] === state[c]) {
        return state[a];
      }
    }
    if (state.every((square) => square !== null)) {
      return "No one";
    }
    return false;
  };
  const isWinner = checkWinner();
  const handleClick = (index) => {
    console.log("clicked", index);
    if (state[index] !== null) {
      return;
    }
    const copyState = [...state];
    copyState[index] = isXTurn ? "X" : "0";
    setState(copyState);
    setIsXTurn(!isXTurn);
  };
  const resetHandler = () => {
    setState(Array(9).fill(null));
    setPlayer1("");
    setPlayer2("");
    setIsPlaying(false);
    setGameStarted(false);
  };

  // const handleClick = (index) => {
  //   console.log("clicked", index);
  //   const updatedState = state.map((value, i) =>
  //     i === index ? (isXTurn ? "X" : "0") : value
  //   );
  //   setState(updatedState);
  //   setIsXTurn(!isXTurn);
  // };

  const startGame = () => {
    if (player1 && player2) {
      setGameStarted(true);
    }
  };
  return (
    <>
      {!gameStarted ? (
        <div className="player_container">
          <input
            type="text"
            value={player1}
            onChange={(e) => setPlayer1(e.target.value)}
            placeholder="Player 1"
          />
          <input
            type="text"
            value={player2}
            onChange={(e) => setPlayer2(e.target.value)}
            placeholder="Player 2"
          />
          <button onClick={startGame}>Play Game</button>
        </div>
      ) : (
        <>
          {isWinner && isWinner !== "No one" ? (
            <div className="who_win">
              {isWinner === "X" ? player1 : player2} won the game
              <button className="palyagin_button" onClick={resetHandler}>
                Play Again
              </button>
            </div>
          ) : isWinner === "No one" ? (
            <div className="who_win">
              No one wins the game
              <button className="palyagin_button" onClick={resetHandler}>
                Play Again
              </button>
            </div>
          ) : (
            <>
              <h4>
                Player{" "}
                {isWinner
                  ? isXTurn
                    ? player2
                    : player1
                  : isXTurn
                  ? player1
                  : player2}{" "}
                Turn
              </h4>

              <div className="board_container">
                <div className="board_row">
                  <Square
                    onClick={() => {
                      handleClick(0);
                    }}
                    value={state[0]}
                  />
                  <Square
                    onClick={() => {
                      handleClick(1);
                    }}
                    value={state[1]}
                  />
                  <Square
                    onClick={() => {
                      handleClick(2);
                    }}
                    value={state[2]}
                  />
                </div>
                <div className="board_row">
                  <Square
                    onClick={() => {
                      handleClick(3);
                    }}
                    value={state[3]}
                  />
                  <Square
                    onClick={() => {
                      handleClick(4);
                    }}
                    value={state[4]}
                  />
                  <Square
                    onClick={() => {
                      handleClick(5);
                    }}
                    value={state[5]}
                  />
                </div>
                <div className="board_row">
                  <Square
                    onClick={() => {
                      handleClick(6);
                    }}
                    value={state[6]}
                  />
                  <Square
                    onClick={() => {
                      handleClick(7);
                    }}
                    value={state[7]}
                  />
                  <Square
                    onClick={() => {
                      handleClick(8);
                    }}
                    value={state[8]}
                  />
                </div>
              </div>
            </>
          )}
        </>
      )}
    </>
  );
};

export default Board;
